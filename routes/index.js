var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');

// setup secret key for JWT encoding
var secret = 'Arbitrary';


/* 檢查 payload 中 timestamp 來看是否過期
 * status:
 * 1 - success
 * 2 - token expired
 * 3 - authentication failed
 * 4 - not enough or too many segments
 */
function checkToken(req, res){
  // parse payload with jwt.decode(token, secret);
  var token = req.query.token;
  try{
    var payload = jwt.decode(token, secret);
    // console.log(payload);
  } catch (err){
    console.log(err)
    return 4
  }

  var issued_time = payload.timestamp
  if((Date.now() - issued_time) > 259200000){ // 2 days = 259200000 miliseconds
    return 2
  } else {
    // auth api_key
    var session_user = req.session.user
    var session_api_key = req.session.api_key
    var api_key = payload.api_key
    if(session_user != null && api_key == session_api_key){
      return 1
    } else {
      return 3
    }
  }  
}


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* POST login and have the API_KEY return */
// router.post('/login', (req, res)=>{
router.get('/login', (req, res)=>{  // for test only

  // check if login successed
  var uid = req.query.uid   // req.body.uid
  var pwd = req.query.pwd   // req.body.pwd

  // TODO: 這裡產生 api_key 的方式請套用自己的算法，可根據用戶選取的不同 API 組合搭配隨機亂數產生
  var payload = { 
    'timestamp': Date.now(),  // timestamp (token generated time)
    'api_key': 'aa7f8d0a95c'  // web API key
  };  

  // 用 session 紀錄該 user 的 API key
  req.session.user = uid
  req.session.api_key = payload.api_key

  var token = jwt.encode(payload, secret);  

  // token 回傳後，user 若要存取 API 需要夾帶此 token 參數
  res.send({ status: 1, token: token })
});

/* GET test API */
router.get('/api/names', (req, res)=>{
  var result = checkToken(req, res)
  if(result == 1){
    // return name list if authorization passed
    var names = ["John", "Anna", "Jack"]
    res.send({ status: 1, name_list: names })
  } else if(result == 2){
    res.send({ status: 2, error: 'Token expired' })
  } else if(result == 3){
    res.send({ status: 3, error: 'Authentication failed' })
  } else if (result == 4){
    res.send({ status: 4, error: 'Not enough or too many segments' })
  }
})


module.exports = router;
